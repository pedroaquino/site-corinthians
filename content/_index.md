## Bem-vindo ao Portal da Fiel, um site sobre o Sport Club Corinthians Paulista!

<img src="https://i0.wp.com/dreamleaguesoccer.com.br/wp-content/uploads/2016/11/escudo-Corinthians.png?fit=512%2C512&ssl=1" width="200" height="200" >

# Aqui você poderá saber mais sobre a história do clube, se informar sobre os últimos jogos e ainda ter acesso aos jogadores que fazem parte do elenco atual!
