---
title : 'COR x GOI'
date : 2023-09-24T13:11:37-03:00
draft : false
image: imagens/goias.png
order: 5
---
# Detalhes da Partida:

**Data:** 26 de Agosto de 2023<br>

**Local:** Arena Corinthians<br>

**Placar Final:** Corinthians 1 - 1 Goiás <br>

## Gols

GUILHERME (59 MINUTOS) - Goiás <br>

MAYCON (82 MINUTOS) - Corinthians <br>

## Estatísticas
20  -  **Chutes**  -  19 <br>

67%  -  **Posse de Bola**  -  33% <br>

628  -  **Passes**  -  343 <br>

16  -  **Faltas**  -  15 <br>

3  -  **Cartões amarelos**  -  1 <br>

0  -  **Cartões Vermelhos**  -  0 <br>

0  -  **Impedimentos**  -  0 <br>

7  -  **Escanteios** -   6 <br>