---
title : 'COR x PAL'
date : 2023-09-24T13:11:37-03:00
draft : false
image: imagens/palmeiras.png
order: 4
---
# Detalhes da Partida:

**Data:** 3 de Setembro de 2023<br>

**Local:** Arena Corinthians<br>

**Placar Final:** Corinthians 0 - 0 Palmeiras <br>

## Gols

SEM GOLS

## Estatísticas
6  -  **Chutes**  -  16 <br>

43%  -  **Posse de Bola**  -  57% <br>

380  -  **Passes**  -  474 <br>

13  -  **Faltas**  -  17 <br>

1  -  **Cartões amarelos**  -  2 <br>

1  -  **Cartões Vermelhos**  -  1 <br>

2  -  **Impedimentos**  -  1 <br>

2  -  **Escanteios** -   11 <br>