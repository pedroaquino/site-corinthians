---
title : 'INT x COR'
date : 2023-09-24T13:11:37-03:00
draft : false
image: imagens/internacional.png
order: 8
---

# Detalhes da Partida:

**Data:** 13 de Agosto de 2023<br>

**Local:** Beira Rio<br>

**Placar Final:** Internacional 2 x 2 Corinthians  <br>

## Gols

RENATO AUGUSTO (13 MINUTOS) - Corinthians <br>

BRUNO HENRIQUE (17 MINUTOS) - Internacional <br>

FABIO SANTOS (86 MINUTOS) - Corinthians <br>

LUIZ ADRIANO (90+9 MINUTOS) - internacional <br>

## Estatísticas
15  -  **Chutes**  -  9 <br>

56%  -  **Posse de Bola**  -  44% <br>

648  -  **Passes**  -  517 <br>

18  -  **Faltas**  -  7 <br>

3  -  **Cartões amarelos**  -  3 <br>

1  -  **Cartões Vermelhos**  -  0 <br>

0  -  **Impedimentos**  -  2 <br>

1  -  **Escanteios** -   4 <br>