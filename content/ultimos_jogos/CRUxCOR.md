---
title : 'CRU x COR'
date : 2023-09-24T13:11:37-03:00
draft : false
image: imagens/cruzeiro.png
order: 6
---

# Detalhes da Partida:

**Data:** 22 de Setembro de 2023<br>

**Local:** Mineirão<br>

**Placar Final:** Cruzeiro 1 - 1 Corinthians  <br>

## Gols

RAFAEL ELIAS (45 MINUTOS) - Cruzeiro <br> 

GUSTAVO SILVA (90+8 MINUTOS) - Corinthians <br>

## Estatísticas
19  -  **Chutes**  -  5 <br>

46%  -  **Posse de Bola**  -  54% <br>

399  -  **Passes**  -  480 <br>

17  -  **Faltas**  -  13 <br>

3  -  **Cartões amarelos**  -  3 <br>

0  -  **Cartões Vermelhos**  -  0 <br>

1  -  **Impedimentos**  -  1 <br>

10  -  **Escanteios** -   3 <br>