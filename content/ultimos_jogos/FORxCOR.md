---
title : 'FOR X COR'
date : 2023-09-24T13:11:37-03:00
draft : false
image: imagens/fortaleza.png
order: 3
---


# Detalhes da Partida:

**Data:** 13 de Agosto de 2023<br>

**Local:** Arena Castelão <br>

**Placar Final:** Fortaleza 2 - 1 Corinthians <br>

## Gols

JUAN MARTIN (26 MINUTOS) - Fortaleza <br>

PEDRINHO (33 MINUTOS) - Corinthians <br>

YAGO PIAKCHU (90+4 MINUTOS) - Fortaleza <br>

## Estatísticas
23  -  **Chutes**  -  6 <br>

56%  -  **Posse de Bola**  -  44% <br>

648  -  **Passes**  -  517 <br>

14  -  **Faltas**  -  17 <br>

1  -  **Cartões amarelos**  -  2 <br>

0  -  **Cartões Vermelhos**  -  0 <br>

2  -  **Impedimentos**  -  1 <br>

6  -  **Escanteios** -   3 <br>