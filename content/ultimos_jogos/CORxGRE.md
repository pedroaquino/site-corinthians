---
title : 'COR x GRE'
date : 2023-09-24T13:11:53-03:00
draft : false
image: imagens/gremio.png
order: 2

---
# Detalhes da Partida:

**Data:** 18 de Setembro de 2023<br>

**Local:** Arena Corinthians<br>

**Placar Final:** Corinthians 4 - 4 Grêmio <br>

## Gols

NATHAN (21 MINUTOS) - Grêmio <br>

CRISTALDO (27 MINUTOS) - Grêmio <br>

FÁBIO SANTOS (45 MINUTOS) - Corinthians <br>

LUCAS VERÍSSIMO(45+4 MINUTOS) - Corinthians <br>

YURI ALBERTO (45+6 MINUTOS) - Corinthians <br>

EVERTON (51 MINUTOS) - Grêmio <br>

SUÁREZ (58 MINUTOS) - Grêmio <br>

GIULIANO (67 MINUTOS) - Corinthians <br>


## Estatísticas
21 -   **Chutes**   - 16 <br>

55%  -  **Posse de Bola**  -  45% <br>

511  -  **Passes** -  431 <br>

17  -  **Faltas**  -  10 <br>

1 - **Cartões amarelos**   - 1 <br>

0  -  **Cartões Vermelhos**  -  0 <br>

1   - **Impedimentos**  -  1 <br>

6  -  **Escanteios**   - 4 <br>