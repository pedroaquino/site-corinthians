---
title : 'COR x BOT'
date : 2023-09-24T13:11:37-03:00
draft : false
image: imagens/botafogo.png
order: 1
---
# Detalhes da Partida:

**Data:** 22 de Setembro de 2023<br>

**Local:** Arena Corinthians<br>

**Placar Final:** Corinthians 1 - 0 Botafogo <br>

## Gols

GIL (59 MINUTOS) - Corinthians

## Estatísticas
19  -  **Chutes**  -  9 <br>

67%  -  **Posse de Bola**  -  33% <br>

668  -  **Passes**  -  342 <br>

18  -  **Faltas**  -  8 <br>

4  -  **Cartões amarelos**  -  4 <br>

0  -  **Cartões Vermelhos**  -  1 <br>

2  -  **Impedimentos**  -  1 <br>

6  -  **Escanteios** -   1 <br>

