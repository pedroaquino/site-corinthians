---
title : 'COR x VAS'
date : 2023-09-24T13:11:37-03:00
draft : false
image: imagens/vasco.png
order: 9
---
# Detalhes da Partida:

**Data:** 29 de Julho de 2023<br>

**Local:** Arena Corinthians<br>

**Placar Final:** Corinthians 3 - 1 Vasco <br>

## Gols

MAYCON (19 MINUTOS) - Corinthians <br>

YURI ALBERTO (62 MINUTOS) - Corinthians <br>

GABRIEL PEC (71 MINUTOS) - Vasco <br>

ROGER GUEDES (73 MINUTOS) - Corinthians <br>


## Estatísticas
12  -  **Chutes**  -  23 <br>

56%  -  **Posse de Bola**  -  44% <br>

648  -  **Passes**  -  517 <br>

12  -  **Faltas**  -  11 <br>

2  -  **Cartões amarelos**  -  0 <br>

0  -  **Cartões Vermelhos**  -  0 <br>

0  -  **Impedimentos**  -  0 <br>

0  -  **Escanteios** -   6 <br>