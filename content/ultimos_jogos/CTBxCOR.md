---
title : 'COR X CTB'
date : 2023-09-24T13:11:37-03:00
draft : false
image: imagens/coritiba.png
order: 7
---

# Detalhes da Partida:

**Data:** 13 de Agosto de 2023<br>

**Local:** Arena Corinthians<br>

**Placar Final:** Corinthians 3 - 1 Coritiba <br>

## Gols

GIL (51 MINUTOS) - Corinthians <br>

MURILLO (28 MINUTOS) - Coritiba <br>

YURI ALBERTO (54 MINUTOS) - Corinthians <br>

WESLEY (67 MINUTOS) - Corinthians <br>

## Estatísticas
17  -  **Chutes**  -  12 <br>

56%  -  **Posse de Bola**  -  44% <br>

648  -  **Passes**  -  517 <br>

17  -  **Faltas**  -  13 <br>

2  -  **Cartões amarelos**  -  2 <br>

0  -  **Cartões Vermelhos**  -  0 <br>

2  -  **Impedimentos**  -  0 <br>

4  -  **Escanteios** -   2 <br>