---
title : 'História'
date : 2023-09-23T22:21:58-03:00
draft : false
---

# História do Sport Club Corinthians Paulista

O Sport Club Corinthians Paulista, é um dos clubes de futebol mais tradicionais e populares do futebol Brasileiro. Fundado em 1º de setembro de 1910 por um grupo de operários no bairro Bom Retiro, na cidade de São Paulo, o clube hoje conta com mais de 30 milhões de torcedores, uma das maiores torcidas do mundo, conhecida popularmente como "a Fiel Torcida". 

# Fundação e Primeiros Anos:

O Corinthians teve sua fundação como resultado de uma reunião entre operários do bairro Bom Retiro que jogavam futebol. O nome "Corinthians" foi inspirado no clube Corinthian Football Club da cidade de Londres.
O primeiro jogo oficial do Corinthians ocorreu em 1910 contra o Estrela Polar, terminando em um empate de 0-0.

# O Estádio do Parque São Jorge:

O Parque São Jorge, inaugurado em 1928, tornou-se a casa do Corinthians por muitos anos. O estádio testemunhou conquistas memoráveis, incluindo o primeiro título paulista em 1914.

<img src="https://www.estadios.net/wp-content/uploads/2019/06/estadio-parque-sao-jorge.jpg"  class=fotos>

# Principais Títulos:

**Campeonato Paulista** <br>
30 títulos (maior vencedor da competição): 1914, 1916, 1922, 1923, 1924, 1928, 1929, 1930, 1937, 1938, 1939, 1941, 1951, 1952, 1954, 1977, 1979, 1982, 1983, 1988, 1995, 1997, 1999, 2001, 2003, 2009, 2013, 2017, 2018 e 2019.

**Copa do Brasil**<br> 
3 títulos: 1995, 2002 e 2009.

**Campeonato Brasileiro** <br>
7 títulos: 1990, 1998, 1999, 2005, 2011, 2015 e 2017

**Copa Libertadores** <br>
1 título: 2012.

**Mundial de Clubes** <br>
2 t´titulos: 2000 e 2012.

# A Fiel Torcida:
A Fiel Torcida é amplamente reconhecida como a maior torcida de futebol do Brasil e uma das maiores do mundo. Sua base de torcedores é vasta e leal, abraçando pessoas de todas as classes sociais e origens. Esteja o Corinthians em uma fase de sucesso ou enfrentando desafios, a Fiel está sempre presente nas arquibancadas, apoiando seu time de maneira apaixonada.

<img src = "https://centraldotimao.com.br/wp-content/uploads/2023/04/corinthians-v-cruzeiro-brasileirao-2023-9-1024x683.jpg" class=fotos>
