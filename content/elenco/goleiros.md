---
title : 'GOLEIROS'
date : 2023-09-24T13:11:37-03:00
draft : false
image: https://static.corinthians.com.br/uploads/1674226763f1981e4bd8a0d6d8462016d2fc6276b3.png
order: 1
---
## Cássio
**Número:** 12 <br>
**Idade:** 36 anos

<img src="https://static.corinthians.com.br/uploads/1674226763f1981e4bd8a0d6d8462016d2fc6276b3.png" class = jogadores>

## Carlos Miguel
**Número:** 22 <br>
**Idade:** 24 anos

<img src="https://static.corinthians.com.br/uploads/167422673120d039f53b4a6786c21ee0dbcd2d2c5d.png" class = jogadores>

## Matheus Donelli
**Número:** 32 <br>
**Idade:** 21 anos

<img src="https://static.corinthians.com.br/uploads/16742267907de6cd35982b5384abd11277d1c25f4f.png" class = jogadores>