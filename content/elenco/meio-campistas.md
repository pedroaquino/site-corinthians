---
title : 'MEIO-CAMPISTAS'
date : 2023-09-24T13:11:37-03:00
draft : false
image: https://static.corinthians.com.br/uploads/1681737834a9c397afa342c368ba24e7620ee41a94.png
order: 4
---
## Renato Augusto
**Número:** 8 <br>
**Idade:** 35 anos

<img src="https://static.corinthians.com.br/uploads/1681737834a9c397afa342c368ba24e7620ee41a94.png" class = jogadores>

## Giuliano
**Número:** 20 <br>
**Idade:** 33 anos

<img src="https://static.corinthians.com.br/uploads/168173823562ac9cd1eac1b6b1d204d458ee016173.png" class = jogadores>

## Matías Rojas
**Número:** 10 <br>
**Idade:** 27 anos

<img src="https://static.corinthians.com.br/uploads/16893687514f52c16c93e61c9e1c25f529932b0071.png" class = jogadores>

## Maycon
**Número:** 7 <br>
**Idade:** 26 anos

<img src="https://static.corinthians.com.br/uploads/168173780196b250a90d3cf0868c83f8c965142d2a.png" class = jogadores>

## Fausto Vera
**Número:** 5 <br>
**Idade:** 23 anos

<img src="https://static.corinthians.com.br/uploads/1681737731333ac5d90817d69113471fbb6e531bee.png" class = jogadores>

## Ruan Oliveira
**Número:** 33 <br>
**Idade:** 23 anos

<img src="https://static.corinthians.com.br/uploads/16817386978ab7f718012c87aad3887a7d136cdf53.png" class = jogadores>