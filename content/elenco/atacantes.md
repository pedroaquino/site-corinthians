---
title : 'ATACANTES'
date : 2023-09-24T13:11:37-03:00
draft : false
image: "https://static.corinthians.com.br/uploads/16817378677c0f63c15f8749d716ba1ac9121cc1a8.png" 
order: 5

---
## Yuri Alberto
**Número:** 9 <br>
**Idade:** 22 anos


<img src="https://static.corinthians.com.br/uploads/16817378677c0f63c15f8749d716ba1ac9121cc1a8.png" alt = Yuri_Alberto class = jogadores>

## Romero
**Número:** 11 <br>
***Idade:*** 31 anos

<img src="https://static.corinthians.com.br/uploads/16817379281fdc0ee9d95c71d73df82ac8f0721459.png" alt = Romero class = jogadores>

## Gustavo Silva
**Número:** 19 <br>
**Idade:** 26 anos

<img src="https://static.corinthians.com.br/uploads/1681738196100d5d9191f185eeb98d6e291756954a.png" alt = Gustavo_silva class = jogadores>

## Giovane
**Número:** 17 <br>
**Idade:** 19 anos

<img src="https://static.corinthians.com.br/uploads/16817381289a555403384fc12f931656dea910e334.png" alt = Giovane class = jogadores>
