---
title : 'ZAGUEIROS'
date : 2023-09-24T13:11:37-03:00
draft : false
image: https://static.corinthians.com.br/uploads/1690558149c61f571dbd2fb949d3fe5ae1608dd48b.png
order: 2

---
## Lucas Veríssimo
**Número:** 3 <br>
**Idade:** 28 anos

<img src="https://static.corinthians.com.br/uploads/1690558149c61f571dbd2fb949d3fe5ae1608dd48b.png" class = jogadores>

## Bruno Méndez
**Número:** 25 <br>
**Idade:** 24 anos

<img src="https://static.corinthians.com.br/uploads/16817383944e0ccd2b894f717df5ebc12f4282ee70.png" class = jogadores>

## Caetano
**Número:** 14 <br>
**Idade:** 24 anos

<img src="https://static.corinthians.com.br/uploads/1681738057f1e5284674fd1e360873c29337ebe2d7.png" class = jogadores>

## Gil
**Número:** 4 <br>
**Idade:** 36 anos

<img src="https://static.corinthians.com.br/uploads/1681737672bd1354624fbae3b2149878941c60df99.png" class = jogadores>