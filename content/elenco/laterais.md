---
title : 'LATERAIS'
date : 2023-09-24T13:11:37-03:00
draft : false
image: https://static.corinthians.com.br/uploads/1681737760daad8d509446c856e52d79f897232876.png
order: 3
---

## Fábio Santos
**Número:** 6 <br>
**Idade:** 38 anos

<img src="https://static.corinthians.com.br/uploads/1681737760daad8d509446c856e52d79f897232876.png" class = jogadores>

## Fagner
**Número:** 23 <br>
**Idade:** 34 anos

<img src="https://static.corinthians.com.br/uploads/16817383329f0609b9d45dd55bed75f892cf095fcf.png" class = jogadores>

## Matheus Bidu
**Número:** 21 <br>
**Idade:** 24 anos

<img src="https://static.corinthians.com.br/uploads/1681738269d58f36f7679f85784d8b010ff248f898.png" class = jogadores>

## Rafael Ramos
**Número:** 2 <br>
**Idade:** 28 anos

<img src="https://static.corinthians.com.br/uploads/1681737636edb684859b848362ec56904286947614.png" class = jogadores>
